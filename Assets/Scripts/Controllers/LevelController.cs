﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : SingletonMonoBehaviour<LevelController>
{
    [SerializeField] float sectionDistance;
    [SerializeField] LevelSection[] sections;

    private int index;

    public LevelSection CurrentSection { get; private set; }

    public static event Action OnChangeSection = delegate { };

    protected override void Awake()
    {
        base.Awake();

        CurrentSection = sections[0];
    }

    public void ChangeSection()
    {
        index++;

        if (index < sections.Length)
        {
            CurrentSection = sections[index];
            OnChangeSection();
        }
        else
        {
            MatchController.Instance.Finish();
        }
    }

    [Button]
    public void RearrangeSections()
    {
        for (int i = 1; i < sections.Length; i++)
        {
            sections[i].transform.position = sections[0].transform.position + new Vector3(0, 0, sectionDistance) * i;
        }
    }
}
