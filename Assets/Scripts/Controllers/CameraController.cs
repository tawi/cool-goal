﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : SingletonMonoBehaviour<CameraController>
{
    [SerializeField] float speed;

    Vector3 startPosition;
    private Vector3 offSetFromStartPoint;
    private bool isMovingToNextSection;

    protected override void Awake()
    {
        base.Awake();
        startPosition = transform.position;
    }

    private void Start()
    {
        offSetFromStartPoint = (transform.position - LevelController.Instance.CurrentSection.StartPoint.transform.position);
    }

    private void OnEnable()
    {
        MatchController.OnResetGame += MatchController_OnResetGame;
        LevelController.OnChangeSection += LevelController_OnChangeSection;
    }

    private void OnDisable()
    {
        MatchController.OnResetGame -= MatchController_OnResetGame;
        LevelController.OnChangeSection -= LevelController_OnChangeSection;
    }

    private void FixedUpdate()
    {
        if (isMovingToNextSection)
        {
            Vector3 targetStartPointPosition = LevelController.Instance.CurrentSection.StartPoint.position;
            Vector3 targetPosition = targetStartPointPosition + offSetFromStartPoint;

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed);

            float distance = Vector3.Distance(transform.position, targetPosition);

            if (distance < 0.1f)
            {
                isMovingToNextSection = false;
            }
        }
    }

    public void MatchController_OnResetGame()
    {
        transform.position = startPosition;
    }

    public void LevelController_OnChangeSection()
    {
        isMovingToNextSection = true;
    }
}
