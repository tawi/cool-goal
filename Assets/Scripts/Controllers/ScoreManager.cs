﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
{    
    public static int Score { get; private set; }

    public static event Action<int> OnScoreChanged = delegate { };

    private void OnEnable()
    {
        Score = 0;
        Projectile.OnGateAchieved += Projectile_OnGateAchieved;
        MatchController.OnResetGame += MatchController_OnResetGame;
    }

    private void OnDisable()
    {
        Projectile.OnGateAchieved -= Projectile_OnGateAchieved;
        MatchController.OnResetGame -= MatchController_OnResetGame;
    }

    private void MatchController_OnResetGame()
    {
        //Score = 0;
        //OnScoreChanged(Score);
    }

    private void Projectile_OnGateAchieved()
    {
        Score++;
        OnScoreChanged(Score);
    }
}
