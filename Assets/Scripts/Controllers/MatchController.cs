﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatchController : SingletonMonoBehaviour<MatchController>
{
    [SerializeField] float failDelayTime = 6f;
    [SerializeField] float successDelayTime = 2f;

    private bool timerIsEnabled;
    private bool isSuccess;
    private float disableTimerTime;

    public static event Action OnFinishGame = delegate { };
    public static event Action OnResetGame = delegate { };

    private void OnEnable()
    {
        Projectile.OnGateAchieved += Projectile_OnGateAchieved;
        Projectile.OnHit += Projectile_OnHit;
        LevelController.OnChangeSection += LevelController_OnChangeSection;
    }

    private void OnDisable()
    {
        Projectile.OnGateAchieved -= Projectile_OnGateAchieved;
        Projectile.OnHit -= Projectile_OnHit;
        LevelController.OnChangeSection -= LevelController_OnChangeSection;
    }

    public void Finish()
    {
        SceneManager.LoadScene(2);
        OnFinishGame();
        Reset();
    }

    public void Reset()
    {
        OnResetGame();
    }

    private IEnumerator Delay()
    {
        timerIsEnabled = true;

        yield return new WaitWhile(() => Time.time < disableTimerTime);

        timerIsEnabled = false;

        if (isSuccess)
        {
            LevelController.Instance.ChangeSection();
        }
        else
        {
            Finish();
        }
    }

    public void Projectile_OnHit()
    {
        if (!timerIsEnabled)
        {
            disableTimerTime = Time.time + failDelayTime;
            StartCoroutine(Delay());
        }
    }

    public void Projectile_OnGateAchieved()
    {
        isSuccess = true;
        disableTimerTime = Time.time + successDelayTime;

        if (!timerIsEnabled)
        {
            StartCoroutine(Delay());
        }
    }

    private void LevelController_OnChangeSection()
    {
        isSuccess = false;
    }
}
