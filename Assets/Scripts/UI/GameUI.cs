﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUI : SingletonMonoBehaviour<GameUI>
{
    [SerializeField] TextMeshProUGUI scoreText;

    private void OnEnable()
    {
        ScoreManager.OnScoreChanged += ScoreManager_OnScoreChanged;
    }

    private void OnDisable()
    {
        ScoreManager.OnScoreChanged -= ScoreManager_OnScoreChanged;
    }

    public void ScoreManager_OnScoreChanged(int score)
    {
        scoreText.SetText(score.ToString());
    }
}
