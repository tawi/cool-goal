﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ScoreMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textScore;

    private void OnEnable()
    {
        textScore.SetText(ScoreManager.Score.ToString());
    }

    public void RestartButton_OnRestartClick()
    {
        SceneManager.LoadScene(1);
    }
}
