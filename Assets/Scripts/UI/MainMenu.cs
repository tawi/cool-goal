﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void StartButton_OnStartClick()
    {
        SceneManager.LoadScene(1);
    }
}
