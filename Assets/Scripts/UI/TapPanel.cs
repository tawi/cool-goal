﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapPanel : SingletonMonoBehaviour<TapPanel>
{
    [SerializeField] float lerpSpeed;

    private Vector2 currentPointer;

    public bool IsClicked { get; private set; }
    public Vector2 StartPosition { get; private set; }
    public Vector2 FinishPosition { get; private set; }

    private void Update()
    {
        if (IsClicked)
        {
            currentPointer = Vector2.Lerp(currentPointer, Input.mousePosition, lerpSpeed * Time.deltaTime);
            FinishPosition = currentPointer;
        }
    }

    public void OnPointerClick(bool isClicked)
    {
        if (!IsClicked)
        {
            StartPosition = Input.mousePosition;
            currentPointer = StartPosition;
        }

        IsClicked = isClicked;
    }
}
