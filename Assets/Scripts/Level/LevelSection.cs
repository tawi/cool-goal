﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSection : MonoBehaviour
{
    [SerializeField] ArcLine middleArcLine;
    [SerializeField] ArcLine gateArcLine;
    [SerializeField] Transform startPoint;

    public ArcLine MiddleArcLine { get => middleArcLine; }
    public ArcLine GateArcLine { get => gateArcLine; }
    public Transform StartPoint { get => startPoint; }
}
