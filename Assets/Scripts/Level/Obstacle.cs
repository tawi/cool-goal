﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private const string OBTACLE_TAG = "Obstacle";

    [SerializeField] float movementSpeed = 5f;
    [SerializeField] Vector3[] wayPoints;
    [SerializeField] int index;

    private Vector3 startPosition;
    private Rigidbody rb;

    private bool isHitted;

    private void Awake()
    {
        startPosition = transform.position.Flat();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (!isHitted && wayPoints?.Length > 0)
        {
            Vector3 direction = (startPosition + wayPoints[index] - transform.position.Flat()).normalized;

            rb.velocity = direction * movementSpeed;
            //transform.position += direction * movementSpeed * Time.fixedDeltaTime;

            float distance = Vector3.Distance(transform.position.Flat(), wayPoints[index] + startPosition);

            if (distance < 0.1f)
            {
                index = (index + 1) % wayPoints.Length;
            }
        }
    }

    public void Hit()
    {
        isHitted = true;
        rb.velocity = Vector3.zero;
        rb.useGravity = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(OBTACLE_TAG))
        {
            Hit();
        }
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < wayPoints?.Length; i++)
        {
            Gizmos.DrawSphere(transform.position + wayPoints[i], 0.1f);
        }
    }
}
