﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcLine : MonoBehaviour
{
    [SerializeField] GameObject leftStartArcPoint;
    [SerializeField] GameObject rightStartArcPoint;

    public GameObject LeftStartArcPoint { get => leftStartArcPoint; }
    public GameObject RightStartArcPoint { get => rightStartArcPoint; }
    public float Distance { get; private set; }

    private void Awake()
    {
        Distance = Vector3.Distance(LeftStartArcPoint.transform.position, RightStartArcPoint.transform.position);
    }

    public Vector3 GetPoint(float t)
    {
        return Vector3.Lerp(LeftStartArcPoint.transform.position, RightStartArcPoint.transform.position, t);
    }
}
