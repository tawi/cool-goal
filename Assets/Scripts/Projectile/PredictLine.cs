﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictLine : SingletonMonoBehaviour<PredictLine>
{
    [SerializeField] GameObject circlePrefab;

    private List<GameObject> circles = new List<GameObject>();
    private bool isDisabled;

    public bool IsDisabled
    {
        get
        {
            return isDisabled;
        }
        set
        {
            if (value != isDisabled)
            {
                isDisabled = value;

                for (int i = 0; i < circles.Count; i++)
                {
                    circles[i].gameObject.SetActive(isDisabled);
                }
            }
        }
    }

    public void Initialize(int circlesCount)
    {
        for (int i = 0; i < circlesCount; i++)
        {
            var circle = Instantiate(circlePrefab, transform);
            circles.Add(circle);
        }

        isDisabled = true;
    }

    public void Draw(int index, Vector3 position)
    {
        circles[index].transform.position = position;
    }
}
