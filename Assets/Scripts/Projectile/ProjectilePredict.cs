﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePredict : MonoBehaviour
{
    [SerializeField] int stepsCount = 8;
    [SerializeField] float sensivity = 5f;
    [SerializeField] float minDrawDistance = 5f;
    [SerializeField] float middleMaxOffset = 20f;
    [SerializeField] float gateMaxOffset = 20f;
    [SerializeField] Vector3 arcOffset = new Vector3(0.1f, 0, -0.6f);
    [SerializeField] Vector3 gateArcOffset = new Vector3(0.5f, 0, 0);

    private Projectile projectile;
    private bool isClicked;
    private bool isStarted;
    private float lastOffset;

    public int StepsCount { get => stepsCount; }

    private void Awake()
    {
        projectile = GetComponent<Projectile>();
    }

    private void Start()
    {
        PredictLine.Instance.Initialize(stepsCount * 2);
    }

    private void OnEnable()
    {
        LevelController.OnChangeSection += LevelController_OnChangeSection;
    }

    private void OnDisable()
    {
        LevelController.OnChangeSection -= LevelController_OnChangeSection;
    }

    private void Update()
    {
        bool shouldCalculate = false;

        if (!isStarted)
        {
            float drawDistance = Vector2.Distance(TapPanel.Instance.StartPosition, TapPanel.Instance.FinishPosition);

            bool lineIsLongEnough = drawDistance > minDrawDistance;
            shouldCalculate = TapPanel.Instance.IsClicked && !projectile.IsFlying;

            PredictLine.Instance.IsDisabled = shouldCalculate && lineIsLongEnough;
        }

        if (shouldCalculate)
        {
            isClicked = true;
            Vector3 previousPoint = LevelController.Instance.CurrentSection.StartPoint.transform.position;

            lastOffset = TapPanel.Instance.FinishPosition.x - TapPanel.Instance.StartPosition.x;
            float clickOffset;
            Vector3 middleArcPoint, gateArcPoint;
            CalculateTargetPoints(out clickOffset, out middleArcPoint, out gateArcPoint);

            previousPoint = DrawArc(previousPoint, clickOffset, middleArcPoint, arcOffset, 0);
            previousPoint = DrawArc(previousPoint, clickOffset, gateArcPoint, gateArcOffset, stepsCount);
        }
        else if (isClicked && !isStarted)
        {
            isStarted = true;
            projectile.IsFlying = true;
        }
    }

    public void CalculateTargetPoints(out float clickOffset, out Vector3 middleArcPoint, out Vector3 gateArcPoint)
    {
        float currentOffset = lastOffset * sensivity;

        var currentMiddleOffset = Mathf.Clamp(currentOffset, -middleMaxOffset, middleMaxOffset);
        //var currentGateOffset = Mathf.Clamp(currentOffset, -gateMaxOffset, gateMaxOffset);

        clickOffset = (currentMiddleOffset / middleMaxOffset);

        int sign = currentOffset > 0 ? 1 : -1;
        float additionalGateLerpOffset = Mathf.Abs(currentOffset) > gateMaxOffset ? ((currentOffset / gateMaxOffset) - 1 * sign) : 0;
        additionalGateLerpOffset /= 2;

        additionalGateLerpOffset = Mathf.Clamp(additionalGateLerpOffset, -0.7f, 0.7f);

        float middleLerpPoint = 0.5f + clickOffset / 2;
        float gateLerpPoint = middleLerpPoint - additionalGateLerpOffset;

        gateLerpPoint = Mathf.Clamp(gateLerpPoint, -1f, 1f);

        middleArcPoint = LevelController.Instance.CurrentSection.MiddleArcLine.GetPoint(middleLerpPoint);
        gateArcPoint = LevelController.Instance.CurrentSection.GateArcLine.GetPoint(gateLerpPoint);
    }

    public Vector3 GetTargetPoint(int index, float clickOffset, Vector3 previousPoint, Vector3 arcPoint)
    {
        float t = 0;
        Vector3 vectorArcOffset = Vector3.zero;

        if (index < StepsCount)
        {
            t = (float)index / (float)StepsCount;
            vectorArcOffset = arcOffset;
        }
        else
        {
            t = (float)(index - StepsCount) / (float)StepsCount;
            vectorArcOffset = gateArcOffset;
        }

        Vector3 localArcOffset = new Vector3(vectorArcOffset.x * clickOffset, 0, vectorArcOffset.z * Mathf.Abs(clickOffset));
        Vector3 middlePoint = Vector3.Lerp(previousPoint, arcPoint, 0.5f) + localArcOffset;

        Vector3 targetPoint = Bezier.CalculateBezierPoint(t, previousPoint, middlePoint, arcPoint);

        return targetPoint;
    }

    private Vector3 DrawArc(Vector3 previousPoint, float clickOffset, Vector3 arcPoint, Vector3 arcOffset, int drawOffsetIndex)
    {
        Vector3 localArcOffset = new Vector3(arcOffset.x * clickOffset, 0, arcOffset.z * Mathf.Abs(clickOffset));
        Vector3 middlePoint = Vector3.Lerp(previousPoint, arcPoint, 0.5f) + localArcOffset;

        for (int step = 0; step < stepsCount; step++)
        {
            float t = (float)step / (float)stepsCount;

            var nextPoint = Bezier.CalculateBezierPoint(t, previousPoint, middlePoint, arcPoint);
            PredictLine.Instance.Draw(drawOffsetIndex + step, nextPoint);

            if (step == stepsCount - 1)
            {
                previousPoint = nextPoint;
            }
        }

        return previousPoint;
    }

    private void LevelController_OnChangeSection()
    {
        isStarted = false;
        isClicked = false;
    }
}