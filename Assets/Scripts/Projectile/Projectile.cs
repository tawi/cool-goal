﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Projectile : SingletonMonoBehaviour<Projectile>
{
    private const string GATE_TAG = "Gate";
    private const string OBTACLE_TAG = "Obstacle";
    private const float DISTANCE_TO_ACHIEVE_TARGET = 0.2F;

    [SerializeField] float flyingSpeed;
    [SerializeField] float angularVelocityMagnitude;
    [SerializeField] float lerpSpeed;
    [SerializeField] float maxAngularVelocity;
    [SerializeField] LayerMask obstacleLayer;

    private bool isFlying;
    private ProjectilePredict projectilePredict;
    private bool isCollided;
    private Rigidbody rb;

    private float middleClickOffset;
    private Vector3 middleArcPoint, gateArcPoint;
    private Vector3 targetPoint;
    private int index;
    private Vector3 targetVelocity;
    private Vector3 currentVelocity;
    private float radius;
    private TrailRenderer trail;

    public static event Action OnGateAchieved = delegate { };
    public static event Action OnHit = delegate { };

    public bool IsFlying
    {
        get
        {
            return isFlying;
        }

        set
        {
            if (isFlying != value)
            {
                isFlying = value;

                if (value)
                {
                    trail.enabled = true;
                    index = 0;
                    projectilePredict.CalculateTargetPoints(out middleClickOffset, out middleArcPoint, out gateArcPoint);

                    Vector3 startPoint = LevelController.Instance.CurrentSection.StartPoint.position;
                    targetPoint = projectilePredict.GetTargetPoint(index + 1 / projectilePredict.StepsCount, middleClickOffset, startPoint, middleArcPoint);
                }
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();
        projectilePredict = GetComponent<ProjectilePredict>();
        rb = GetComponent<Rigidbody>();
        radius = GetComponent<SphereCollider>().radius;
        trail = GetComponentInChildren<TrailRenderer>();
        rb.maxAngularVelocity = maxAngularVelocity;
    }

    private void OnEnable()
    {
        MatchController.OnResetGame += MatchController_OnResetGame;
        LevelController.OnChangeSection += LevelController_OnChangeSection;
    }

    private void OnDisable()
    {
        MatchController.OnResetGame -= MatchController_OnResetGame;
        LevelController.OnChangeSection -= LevelController_OnChangeSection;
    }

    private void FixedUpdate()
    {
        if (IsFlying && !isCollided)
        {
            bool targetIsAchieved = CheckForTarget();

            SetVelocity();

            CheckForFutureCollision(targetIsAchieved);
        }
    }

    private bool CheckForTarget()
    {
        bool targetIsAchieved = false;

        float distance = Vector3.Distance(targetPoint, transform.position);

        if (distance < DISTANCE_TO_ACHIEVE_TARGET)
        {
            index++;

            targetIsAchieved = index == projectilePredict.StepsCount * 2;

            if (!targetIsAchieved)
            {
                Vector3 arcPoint = Vector3.zero;
                Vector3 localStartPosition = Vector3.zero;

                if (index < projectilePredict.StepsCount)
                {
                    var startPoint = LevelController.Instance.CurrentSection.StartPoint.position;
                    localStartPosition = startPoint;
                    arcPoint = middleArcPoint;
                }
                else
                {
                    localStartPosition = middleArcPoint;
                    arcPoint = gateArcPoint;
                }

                targetPoint = projectilePredict.GetTargetPoint(index, middleClickOffset, localStartPosition, arcPoint);
            }
        }

        return targetIsAchieved;
    }

    private void SetVelocity()
    {
        targetVelocity = (targetPoint - transform.position).normalized;
        currentVelocity = Vector3.Lerp(currentVelocity, targetVelocity, lerpSpeed * Time.fixedDeltaTime);
        rb.velocity = currentVelocity * flyingSpeed;
        rb.angularVelocity += transform.right * angularVelocityMagnitude;
    }

    private void CheckForFutureCollision(bool targetIsAchieved)
    {
        Vector3 nextPosition = transform.position + rb.velocity * Time.fixedDeltaTime;

        RaycastHit hit;

        Physics.SphereCast(nextPosition, radius, currentVelocity.normalized, out hit, radius, obstacleLayer);

        if (hit.collider != null || targetIsAchieved)
        {
            hit.transform?.GetComponent<Obstacle>()?.Hit();

            OnHit();
            isCollided = true;
            rb.useGravity = true;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag(GATE_TAG))
        {
            OnGateAchieved();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(OBTACLE_TAG))
        {
            if (!isCollided)
            {
                OnHit();
                isCollided = true;
                rb.useGravity = true;
            }

            collision.transform.GetComponent<Obstacle>()?.Hit();
        }
    }

    public void MatchController_OnResetGame()
    {
        transform.rotation = Quaternion.identity;
        isFlying = false;
    }

    private void LevelController_OnChangeSection()
    {
        trail.enabled = false;
        transform.position = LevelController.Instance.CurrentSection.StartPoint.position;
        transform.rotation = Quaternion.identity;
        isFlying = false;
        isCollided = false;
        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        currentVelocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}
